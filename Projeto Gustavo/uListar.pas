unit uListar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmListar = class(TForm)
    btListar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure btListarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }

    lbNome: array [0..15] of TLabel;
    lbTelefone: array [0..15] of TLabel;
    lbSetor: array [0..15] of TLabel;
    lbSalario: array [0..15] of TLabel;
    margin: integer;
    marginT: integer;
    marginSe: integer;
    marginSa: integer;

  end;

var
  fmListar: TfmListar;

implementation
uses
  uCreate, uTelaInicial;

{$R *.dfm}

procedure TfmListar.btListarClick(Sender: TObject);
  var
    i: integer;
    a: integer;

  begin
       if btListar.Tag = 1 then
    exit;
 btListar.Tag := 1;

    Randomize;
      margin:= 25;
      marginT:= 25;
      marginSe:= 25;
      marginSa:= 25;
      est[0,4]:= 'Gusttavo';
      est[1,4]:= 'Renato';
      est[2,4]:= 'Renan';
      est[3,4]:= 'Isac';
      est[0,5]:= '999999999';
      est[1,5]:= '999999999';
      est[2,5]:= '999999999';
      est[3,5]:= '999999999';
      est[0,6]:= 'Suporte';
      est[1,6]:= 'Implantação';
      est[2,6]:= 'Desenvolvimento';
      est[3,6]:= 'Desenvolvimento';
      est[0,7]:= IntToStr(400 + Random(600));
      est[1,7]:= IntToStr(400 + Random(600));
      est[2,7]:= IntToStr(400 + Random(600));
      est[3,7]:= IntToStr(400 + Random(600));


      for a := 4 to 15 do
        begin
          est[a,4]:= cdNome[a-4];
          est[a,5]:= cdTelefone[a-4];
          est[a,6]:= cdSetor[a-4];
          est[a,7]:= cdSalario[a-4];
        end;



    for i := 0 to 15 do
      begin
        lbNome[i] := Tlabel.create(application);
        lbNome[i].Parent := fmListar;
        lbNome[i].Caption:= est[i,4];
        lbNome[i].Top := margin;
        lbNome[i].Left := 120;
        margin:= margin + 13;

        lbTelefone[i] := Tlabel.create(application);
        lbTelefone[i].Parent := fmListar;
        lbTelefone[i].Caption:= est[i,5];
        lbTelefone[i].Top := marginT;
        lbTelefone[i].Left := 200;
        marginT:= marginT + 13;

        lbSetor[i] := Tlabel.create(application);
        lbSetor[i].Parent := fmListar;
        lbSetor[i].Caption:= est[i,6];
        lbSetor[i].Top := marginSe;
        lbSetor[i].Left := 288;
        marginSe:= marginSe + 13;

        lbSalario[i] := Tlabel.create(application);
        lbSalario[i].Parent := fmListar;
        lbSalario[i].Caption:= est[i,7];
        lbSalario[i].Top := marginSa;
        lbSalario[i].Left := 392;
        marginSa:= marginSa + 13;
      end;
  end;


procedure TfmListar.FormActivate(Sender: TObject);
begin
  btListar.Tag:= 0;
end;

procedure TfmListar.FormClose(Sender: TObject; var Action: TCloseAction);
var x: integer;
begin
  btListar.Tag:= 0;

  for x := 0 to 15 do
    begin
     lbNome[x].free;
     lbTelefone[x].free;
     lbSetor[x].Free;
     lbSalario[x].free;
    end;

end;

end.
