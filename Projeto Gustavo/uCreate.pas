unit uCreate;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
    TfmCreate = class(TForm)
    Label1: TLabel;
    edNome: TEdit;
    edTelefone: TEdit;
    edSalario: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    btSave: TButton;
    btLimpar: TButton;
    rbDesenvolvimento: TRadioButton;
    rbSuporte: TRadioButton;
    rbImplantacao: TRadioButton;
    procedure btSaveClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure btLimparClick(Sender: TObject);
    procedure edTelefoneKeyPress(Sender: TObject; var Key: Char);
    procedure edSalarioKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Private declarations }

  end;

var
  fmCreate: TfmCreate;

implementation
uses
  uTelaInicial, uListar;

function  Ret_Numero(Key: Char; Texto: string; EhDecimal: Boolean = False): Char;
  begin
    if  not EhDecimal then
      begin
        if  not ( Key in ['0'..'9', Chr(8)] ) then
        Key := #0
        end
    else
      begin
        if  Key = #46 then
          Key := DecimalSeparator;
        if  not ( Key in ['0'..'9', Chr(8), DecimalSeparator] ) then
          Key := #0
        else if  ( Key = DecimalSeparator ) and ( Pos( Key, Texto ) > 0 ) then
          Key := #0;
      end;
    Result := Key;
  end;

{$R *.dfm}

procedure TfmCreate.btLimparClick(Sender: TObject);
begin
  edNome.Text:= '';
  edTelefone.Text:= '';
  edSalario.Text:= '';
  rbDesenvolvimento.checked:= false;
  rbSuporte.checked:= false;
  rbImplantacao.checked:= false;
  edNome.setfocus;

end;

procedure TfmCreate.btSaveClick(Sender: TObject);
var
  a: integer;
begin
  if edNome.text = '' then
    begin
      ShowMessage('Informe o Nome do Estagiário!');
      edNome.setFocus;
      exit;
    end;

  if edTelefone.text = '' then
    begin
      ShowMessage('Informe o Telefone do Estagiário!');
      edTelefone.setFocus;
      exit;
    end;

  if rbDesenvolvimento.checked or rbSuporte.checked or rbImplantacao.checked = false then
    begin
     showMessage('Informe o Setor');
     rbDesenvolvimento.SetFocus;;
     exit;
    end;

    if edSalario.text = '' then
    begin
      ShowMessage('Informe o Salário do Estagiário!');
      edSalario.setFocus;
      exit;
    end;

    for a := 10 downto 1 do
      begin
        cdNome[a]:= cdNome[a-1];
        cdTelefone[a]:= cdTelefone[a-1];
        cdSetor[a]:= cdSetor[a-1];
        cdSalario[a]:= cdSalario[a-1];
      end;

    cdNome[0]:= edNome.text;
    cdTelefone[0]:= edTelefone.text;
    if rbDesenvolvimento.checked = true then
      cdSetor[0]:= rbDesenvolvimento.caption
    else if rbSuporte.checked = true then
      cdSetor[0]:= rbSuporte.caption
    else if rbImplantacao.checked = true then
      begin
        cdSetor[0]:= rbImplantacao.caption;
      end;
    cdSalario[0]:= edSalario.text;

  showMessage('Estagiário cadastrado com Sucesso!');
end;

procedure TfmCreate.edSalarioKeyPress(Sender: TObject; var Key: Char);
begin
   Key := Ret_Numero( Key, edSalario.Text );
end;

procedure TfmCreate.edTelefoneKeyPress(Sender: TObject; var Key: Char);
begin
  Key := Ret_Numero( Key, edTelefone.Text );
end;

procedure TfmCreate.FormActivate(Sender: TObject);
begin
//cdNome[0]:= '';
//cdTelefone[0]:= '';
//cdSetor[0]:= '';
//cdSalario[0]:= '';
end;

procedure TfmCreate.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  edNome.Text:= '';
  edTelefone.Text:= '';
  edSalario.Text:= '';
  rbDesenvolvimento.checked:= false;
  rbSuporte.checked:= false;
  rbImplantacao.checked:= false;
end;

end.
