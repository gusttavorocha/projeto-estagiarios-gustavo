object fmPesquisar: TfmPesquisar
  Left = 0
  Top = 0
  Caption = 'fmPesquisar'
  ClientHeight = 222
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object btPesquisar: TButton
    Left = 336
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Pesquisar'
    TabOrder = 1
    OnClick = btPesquisarClick
  end
  object edPesquisar: TEdit
    Left = 8
    Top = 8
    Width = 313
    Height = 21
    TabOrder = 0
  end
  object btLimpar: TButton
    Left = 417
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Limpar'
    TabOrder = 2
    OnClick = btLimparClick
  end
end
