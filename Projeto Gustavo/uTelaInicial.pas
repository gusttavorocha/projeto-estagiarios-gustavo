unit uTelaInicial;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;
var
  cdNome: array[0..15] of string;
  cdTelefone: array[0..15] of string;
  cdSetor: array[0..15] of string;
  cdSalario: array[0..15] of string;
  est: array [0..15,4..7] of string;
  margin: integer;
  marginT: integer;
  marginSe: integer;
  marginSa: integer;
type
  TfmTelaInicial = class(TForm)
    btShow: TButton;
    btPesquisa: TButton;
    btMedia: TButton;
    btCriar: TButton;
    procedure btShowClick(Sender: TObject);
    procedure btCriarClick(Sender: TObject);
    procedure btMediaClick(Sender: TObject);
    procedure btPesquisaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure dados(Sender: TObject);
  end;


var
  fmTelaInicial: TfmTelaInicial;


implementation
uses
  uCreate, uListar, uMedia, uPesquisar;

{$R *.dfm}
procedure TfmTelaInicial.btCriarClick(Sender: TObject);
begin
  fmCreate.show;
end;

procedure TfmTelaInicial.btMediaClick(Sender: TObject);
begin
  fmMedia.Show;
end;

procedure TfmTelaInicial.btPesquisaClick(Sender: TObject);
begin
  fmPesquisar.show;
end;

procedure TfmTelaInicial.btShowClick(Sender: TObject);
begin
  fmListar.show;
end;

procedure TfmTelaInicial.dados(sender: TObject);
  begin

  end;

end.
