program Project1;

uses
  Forms,
  uTelaInicial in 'uTelaInicial.pas' {fmTelaInicial},
  uCreate in 'uCreate.pas' {fmCreate},
  uListar in 'uListar.pas' {fmListar},
  uMedia in 'uMedia.pas' {fmMedia},
  uPesquisar in 'uPesquisar.pas' {fmPesquisar};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmTelaInicial, fmTelaInicial);
  Application.CreateForm(TfmCreate, fmCreate);
  Application.CreateForm(TfmListar, fmListar);
  Application.CreateForm(TfmMedia, fmMedia);
  Application.CreateForm(TfmPesquisar, fmPesquisar);
  Application.Run;
end.
