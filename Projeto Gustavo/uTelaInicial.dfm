object fmTelaInicial: TfmTelaInicial
  Left = 0
  Top = 0
  Caption = 'fmTelaInicial'
  ClientHeight = 244
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btShow: TButton
    Left = 8
    Top = 24
    Width = 169
    Height = 25
    Caption = 'Listar Estagi'#225'rios'
    TabOrder = 0
    OnClick = btShowClick
  end
  object btPesquisa: TButton
    Left = 8
    Top = 55
    Width = 169
    Height = 25
    Caption = 'Pesquisar Estagi'#225'rios'
    TabOrder = 1
    OnClick = btPesquisaClick
  end
  object btMedia: TButton
    Left = 8
    Top = 86
    Width = 169
    Height = 25
    Caption = 'M'#233'dia sal'#225'rial dos Estagi'#225'rios'
    TabOrder = 2
    OnClick = btMediaClick
  end
  object btCriar: TButton
    Left = 8
    Top = 117
    Width = 169
    Height = 25
    Caption = 'Adcionar Estagi'#225'rio'
    TabOrder = 3
    OnClick = btCriarClick
  end
end
