object fmCreate: TfmCreate
  Left = 0
  Top = 0
  Caption = 'fmCreate'
  ClientHeight = 242
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 99
    Height = 13
    Caption = 'Dados do Estagi'#225'rio:'
  end
  object Label2: TLabel
    Left = 8
    Top = 43
    Width = 31
    Height = 13
    Caption = 'Nome:'
  end
  object Label3: TLabel
    Left = 224
    Top = 43
    Width = 46
    Height = 13
    Caption = 'Telefone:'
  end
  object Label4: TLabel
    Left = 8
    Top = 111
    Width = 30
    Height = 13
    Caption = 'Setor:'
  end
  object Label5: TLabel
    Left = 224
    Top = 111
    Width = 36
    Height = 13
    Caption = 'Sal'#225'rio:'
  end
  object edNome: TEdit
    Left = 60
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object edTelefone: TEdit
    Left = 300
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 1
    OnKeyPress = edTelefoneKeyPress
  end
  object edSalario: TEdit
    Left = 300
    Top = 108
    Width = 121
    Height = 21
    TabOrder = 5
    OnKeyPress = edSalarioKeyPress
  end
  object btSave: TButton
    Left = 106
    Top = 200
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 6
    OnClick = btSaveClick
  end
  object btLimpar: TButton
    Left = 324
    Top = 200
    Width = 75
    Height = 25
    Caption = 'Limpar'
    TabOrder = 7
    OnClick = btLimparClick
  end
  object rbDesenvolvimento: TRadioButton
    Left = 51
    Top = 90
    Width = 113
    Height = 17
    Caption = 'Desenvolvimento'
    TabOrder = 2
    TabStop = True
  end
  object rbSuporte: TRadioButton
    Left = 51
    Top = 110
    Width = 113
    Height = 17
    Caption = 'Suporte'
    TabOrder = 3
    TabStop = True
  end
  object rbImplantacao: TRadioButton
    Left = 51
    Top = 128
    Width = 113
    Height = 17
    Caption = 'Implanta'#231#227'o'
    TabOrder = 4
    TabStop = True
  end
end
